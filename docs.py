import os
import time

import requests
import fake_useragent
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.firefox.webdriver import WebDriver
from dotenv import load_dotenv

load_dotenv()

base_dir = os.getcwd()
HEADERS = {'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0', 'accept': '*/*'}
session = requests.Session()
HOST = 'https://store.konecranes.com'
fake_user = fake_useragent.UserAgent().random
header = {
    'user-agent': fake_user
}


def wait_to_load_page(driver: WebDriver) -> None:
    WebDriverWait(driver, 15).until(ec.presence_of_element_located((By.TAG_NAME, "html")))
    time.sleep(0.5)


def autorization() -> WebDriver:
    if os.getenv("system") == 'windows': 
        DRIVER= base_dir + "/geckodriver.exe"
        service = Service(executable_path=DRIVER)
        options = webdriver.FirefoxOptions()
        options.binary_location = r'C:\Program Files\Mozilla Firefox\firefox.exe'
        #options.add_argument(('--headless'))
        driver = webdriver.Firefox(service=service, options=options)
    elif os.getenv("system") == 'linux': 
        DRIVER="geckodriver"
        service = Service(executable_path=DRIVER)
        options = webdriver.FirefoxOptions()
        options.add_argument('Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0')
        options.add_argument(('--headless'))
        driver = webdriver.Firefox(service=service, options=options)
    else:
        print("Укажите операционную систему в файле .env")
        exit()


    driver.get('https://store.konecranes.com/cpc/en/BRAKE-DISC-SET/p?p=c66rYesZ4CC9O4cOd17ZNA%3D%3D_52314611')
    wait_to_load_page(driver)
    driver.find_element(By.XPATH, '/html/body/main/header/nav[2]/div/div/div[4]/div/div/div/ul/li/a').click()
    time.sleep(3)

    email_input = driver.find_element(By.XPATH, '//*[@id="1-email"]')
    email_input.clear()
    email_input.send_keys(os.getenv("mail"))
    password_input = driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div/div[3]/div/div/form/div/div/div/div/div[2]/div[2]/span/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/input')
    password_input.clear()
    password_input.send_keys(os.getenv("password"))
    driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div/div[3]/div/div/form/div/div/div/button/span').click()
    wait_to_load_page(driver)
    time.sleep(3)

    return driver


def get_html(url: str, params: dict=None) -> requests.models.Response:
    r = requests.get(url, headers=header, params=params)
    return r


def get_content(url: str, driver: WebDriver, number: str) -> list:
    driver.get(url)
    wait_to_load_page(driver)
    html_from_selenium = driver.page_source
    soup = BeautifulSoup(html_from_selenium, 'html.parser')
    try:
        serial_number = soup.find('div', class_='motor-results').get_text(strip=True) + '\n'
    except:
        serial_number = None
    try:
        motor_data = soup.find('div', class_='motor__details').get_text() + '\n'
    except:
        motor_data = None
    str_data = serial_number + motor_data
    try:
        os.mkdir(base_dir + '/' + os.getenv("dir_result_name") + '/' + str(number))
    except:
        pass
    FILE_HTML = base_dir + '/' + os.getenv("dir_result_name") + '/' + str(number) + '/html.txt'
    write_html_to_txt_file(FILE_HTML, soup.text.replace('\n\n', '').replace('  ', '\n').replace('\n\n', ''), str_data)
    return str_data


def write_html_to_txt_file(path: str, html_text: str, data: str) -> None:
    with open(path, 'a',  encoding='utf8', newline='') as file:
        data = data.replace('\n\n', '').replace('  ', '\n').replace('\n\n', '')
        try:
            data = data[:data.find('Motor Type')] + '\n' + data[data.find('Motor Type'):] 
        except:
            data = data 
        file.write(data)

        try:
            html_text = html_text[html_text.find('Plate'):] 
        except:
            html_text = html_text[html_text.find('plate'):] 
        file.write('\n\n' + html_text)


def get_data_from_page(URL: str, driver: WebDriver, number: str) -> list:
    print(f'Парсинг страницы {URL}...')
    wait_to_load_page(driver)
    return get_content(URL, driver, number) 


def parse() -> None:
    driver = autorization()
    for number in range(int(os.getenv("from")), int(os.getenv("to"))):
        print(str(number))
        URL = 'https://store.konecranes.com/cpc/en/documents/motor-result?searchAttr=serialNumber&searchTerm=' + str(number) + '&returnUrl=%2Fdocuments&source=NEW'
        get_data_from_page(URL, driver, number) 
    driver.quit()

parse()
